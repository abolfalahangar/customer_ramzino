// const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
//   .BundleAnalyzerPlugin;

// const VueyifyLoaderPlugin = require("vuetify-loader/lib/plugin");

// const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
// const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
// const PreloadWebpackPlugin = require("@vue/preload-webpack-plugin");
const webpack = require("webpack");

module.exports = {
  devServer: {
    disableHostCheck: false,
    port: 3333
  },
  //runtimeCompiler: true,
  configureWebpack: {
    plugins: [new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),// new BundleAnalyzerPlugin(),//new VueyifyLoaderPlugin()
    ],
  },
  publicPath: "/",
  pwa: {
    themeColor: "#42b983",
    msTileColor: "#42b983",
    appleMobileWebAppCache: "yes",
    manifestOptions: {
      background_color: "#42b983"
    },
    workboxOptions: {
      exclude: [/_redirects/]
    }
  },

  transpileDependencies: ["vuetify"]
  // configureWebpack: {
  //   optimization: {
  //     runtimeChunk: "single",
  //     splitChunks: {
  //       chunks: "all",
  //       maxInitialRequests: Infinity,
  //       minSize: 0,
  //       cacheGroups: {
  //         vendor: {
  //           test: /[\\/]node_modules[\\/]/,
  //           name(module) {
  //             const packageName = module.context.match(
  //               /[\\/]node_modules[\\/](.*?)([\\/]|$)/
  //             )[1];
  //             return `npm.${packageName.replace("@", "")}`;
  //           }
  //         }
  //       }
  //     },
  //     minimizer: [
  //       new UglifyJsPlugin({
  //         // Enable file caching
  //         cache: true,
  //         // Use multiprocess to improve build speed
  //         parallel: true
  //       }),
  //       new OptimizeCSSAssetsPlugin({})
  //     ]
  //   },
  //   plugins: [
  //     new BundleAnalyzerPlugin(), // new VueyifyLoaderPlugin()
  //     new PreloadWebpackPlugin({
  //       rel: "preload",
  //       as(entry) {
  //         if (/\.css$/.test(entry)) return "style";
  //         if (/\.woff$/.test(entry)) return "font";
  //         if (/\.png$/.test(entry)) return "image";
  //         return "script";
  //       }
  //     })
  //   ]
  // },
  // css: {
  //   loaderOptions: {
  //     css: {
  //       // 8.0.3
  //       prependData: `@import "@/assets/scss/app/variables.scss";`
  //     }
  //   }
  // }
};


//test git