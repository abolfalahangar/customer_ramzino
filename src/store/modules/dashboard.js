import { make } from "vuex-pathify";

const state = {
  orderBuy: {},
  orderSell: {},
  symbol: 0,
  orders: [],
  marketTabs: 1,
  componentsData:{},
  loader:false,
};

const mutations = make.mutations(state);
const actions = {
  ...make.actions(state),
  getMarket({ commit },pair){
    this._vm.$http.get(`/Market/${pair}`).then((res)=>{
      commit("loader",true);
      console.log("res Market pair",res);
      
      res.data?.sellOrders?.map((item,index)=>{
        item.total = parseInt( item.total)
      })
      
      res.data?.buyOrders?.map((item,index)=>{
        item.total = parseInt( item.total)
      })
      // res.data?.userOpenOrders?.map((item,index)=>{
      //   item.filled = 0
      // })
      commit("componentsData",res.data);  

    }).finally(() => {
      commit("loader",false);
    })
  }
};

const getters = {
  symbolName: state => {
    if (state.symbol == 0) {
      return "BTC";
    } else if (state.symbol == 1) {
      return "DOGE";
    } else if (state.symbol == 2) {
      return "LTC";
    } else if (state.symbol == 3) {
      return "DASH";
    }
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
