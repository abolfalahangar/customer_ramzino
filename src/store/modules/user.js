import { make } from "vuex-pathify";

const state = {
  notifications: [],
  current_user: {},
  token:""
};

const mutations = make.mutations(state);

const actions = {
  getToken({ commit }, user) {},
  register({ commit, dispach }, data) {
    this._vm.$http
      .post("/Auth", data)
      .then(
        res => {
          this._vm.$setDataToLocalStorage(data);
          commit("current_user", data);
          this._vm.$router.replace("/dashboard/BTCTOMAN");
          return res;
        },
        err => {
          this._vm.$showSnackbar({
            content: "متاسفانه ثبت نام انجام نشد دوباره امتحان کنید",
            color: "red"
          });
        }
      )
  },
  init  ({commit}){
    this._vm.$nextTick(() => {
      let user =   this._vm.$getDataFromLocalStorage("user")
      let token =   this._vm.$getDataFromLocalStorage("token")
      commit("current_user",user)
      commit("token",token)
    })
  

  
  }
};

const getters = {
  user_is_authenticated: state => {
    return state.current_user != null;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
