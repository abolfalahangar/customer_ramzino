// Pathify
import { make } from "vuex-pathify";

let theme = localStorage.getItem("theme");
theme = theme == null ? "dark" : theme == "dark" ? "dark" : "light";
// Data
const state = {
  items: [
    {
      title: "داشبورد",
      icon: "mdi-desktop-mac-dashboard",
      to: "/dashboard"
    },
    {
      title: "سفارشات باز",
      icon: "mdi-book-open-blank-variant",
      to: "/open-orders"
    },
    {
      title: "سوابق سفارشات",
      icon: "mdi-clipboard-outline",
      to: "/orders-records"
    },
    {
      title: "تاریخچه معماملات",
      icon: "mdi-bank",
      to: "/trade-history"
    },
    {
      title: "حساب های بانکی من",
      icon: "mdi-bank",
      to: "/my-bank-account"
    },
    {
      title: "کیف پول",
      icon: "mdi-wallet",
      to: "/wallets"
    },
    {
      title: "سوابق واریز",
      icon: "mdi-currency-usd",
      to: "/deposit-records"
    },
    {
      title: "سوابق برداشت",
      icon: "mdi-ballot-recount",
      to: "/withdraw-records"
    },
    {
      title: "تراکنش ها",
      icon: "mdi-handshake",
      to: "/transactions"
    },
    {
      title: "کسب درآمد",
      icon: "mdi-cash-multiple",
      to: "/earn-money"
    }
  ],
  drawer: null,
  content: "",
  color: "",
  position: "",
  marketModal: false,
  theme,
  overlay: false,
  percent: 0,socket:{}
};

const mutations = make.mutations(state);

mutations["showMessage"] = (state, payload) => {
  state.content = payload.content;
  state.color = payload.color;
  state.position = payload.position;
};
const actions = {
  ...make.actions(state),
  init: async ({ dispatch }) => {
    //
  }
};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
