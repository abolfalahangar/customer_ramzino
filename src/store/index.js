// Vue
import Vue from "vue";
import Vuex from "vuex";
import pathify from "@/plugins/vuex-pathify";

// Modules
import * as modules from "./modules";

//////////////////// Functional getters are not cached
Vue.use(Vuex);
const store = new Vuex.Store({
  modules,
  plugins: [pathify.plugin]
});

store.dispatch("user/init");

export default store;

export const ROOT_DISPATCH = Object.freeze({ root: true });

// mounted () {
//   this.$store.registerModule('admin', adminModule)
// },
// beforeDestroy () {
//  this.$store.unregisterModule('admin')
// }
