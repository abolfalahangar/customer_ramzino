import React from "react";
import axios from "axios";
import { makeQueryString, notifyError } from "./Utils";
import ErrorTemp from "../component/ErrosTemp";

const baseUrl = "http://localhost:5007/api/";

export default class restUtil {
  constructor(param) {
    const { url, params, data } = param;
    this.url = url;
    this.params = params;
    this.data = data;
  }

  get() {
    const qs = this.params && `?${makeQueryString(this.params)}`;
    return this.request({
      method: "GET",
      url: qs ? `${this.url}${qs}` : this.url
    });
  }

  post() {
    return this.request({ method: "POST", url: this.url, data: this.data });
  }

  put() {
    return this.request({ method: "PUT", url: this.url, data: this.data });
  }

  remove() {
    return this.request({ method: "DELETE", url: this.url });
  }

  errorHandler(error) {
    if (!error) {
      return Promise.reject();
    }

    if (!error.response) {
      notifyError(error);
      return Promise.reject();
    }

    if (error.response && error.response.status === 401) {
      if (
        error.response.request &&
        error.response.request.responseURL.includes("auth")
      ) {
        notifyError("نام کاربری و یا پسورد اشتباه می باشد");
      } else {
        notifyError("لطفا وارد حساب کاربری شوید");
      }
      return Promise.reject();
    }

    if (error.response && error.response.status === 404) {
      if (error.response.data) {
        notifyError(error.response.data.message);
        return Promise.reject();
      }
      notifyError("آدرس یافت نشد");
      return Promise.reject();
    }

    if (error.response && error.response.status === 400) {
      notifyError(error.response.data.message);
      return Promise.reject();
    }

    if (error.response && error.response.status === 422) {
      if (error.response.data && error.response.data.errors) {
        notifyError(<ErrorTemp data={error.response.data.errors} />);
      }
      return Promise.reject();
    }

    notifyError("خطای فنی رخ داده لطفا دوباره سعی کنید");
    return Promise.reject();
  }

  request(config) {
    let { method, url, headers, data } = config;
    headers = {};

    if (localStorage.getItem("user")) {
      var user = JSON.parse(localStorage.getItem("user"));
      headers.Authorization = `Bearer ${user.auth_token}`;
    }

    if (data && typeof data === "object") {
      data = JSON.stringify(data);
      headers["Content-Type"] = "application/json";
    }

    axios.interceptors.response.use(response => response, this.errorHandler);

    return axios({
      url: `${baseUrl}${url}`,
      method: method,
      headers: headers,
      data: data
    }).catch();
  }
}
