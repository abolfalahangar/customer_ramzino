import moment from "moment-jalaali";
import { toast } from "react-toastify";

export function getCurrentDate() {
  let date = new Date();
  let month = (date.getMonth() + 1).toString();
  month = month.length > 1 ? month : `0${month}`;
  return `${date.getFullYear()}/${month}/${date.getDate()}`;
}

export function getGeorgianDate(date) {
  if (date === "" || !date || date === {}) {
    return undefined;
  }
  return moment(date, "jYYYY/jM/jD").format("YYYY-M-D");
}

export function getPersianDate(date) {
  if (date === "" || !date || date === {}) {
    return undefined;
  }
  return moment(date, "YYYY-M-D").format("jYYYY/jM/jD");
}

export function makeQueryString(params) {
  const esc = encodeURIComponent;
  const query = Object.keys(params)
    .filter(k => !!params[k])
    .map(k => `${esc(k)}=${esc(params[k])}`)
    .join("&");
  return query;
}

export function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

export function isPersianNumber(n) {
  if (n === "" || n === undefined || n === null) {
    return false;
  }
  let result = false;
  let persianNumbers = [
    "\u0660",
    "\u06F0",
    "\u0661",
    "\u06F1",
    "\u0662",
    "\u06F2",
    "\u0663",
    "\u06F3",
    "\u0664",
    "\u06F4",
    "\u0665",
    "\u06F5",
    "\u0666",
    "\u06F6",
    "\u0667",
    "\u06F7",
    "\u0668",
    "\u06F8",
    "\u0669",
    "\u06F9"
  ];
  for (let i = 0; i < n.length; i++) {
    if (persianNumbers.includes(n[i])) {
      result = true;
      break;
    }
  }
  return result;
}

export function toEnNumber(data) {
  if (!data || data === "") {
    return;
  }

  String.prototype.replaceAll = function(search, replacement) {
    let target = this;
    return target.replace(new RegExp(search, "g"), replacement);
  };

  return data
    .replaceAll("\u0660", "0") //٠
    .replaceAll("\u06F0", "0") //۰
    .replaceAll("\u0661", "1") //١
    .replaceAll("\u06F1", "1") //۱
    .replaceAll("\u0662", "2") //٢
    .replaceAll("\u06F2", "2") //۲
    .replaceAll("\u0663", "3") //٣
    .replaceAll("\u06F3", "3") //۳
    .replaceAll("\u0664", "4") //٤
    .replaceAll("\u06F4", "4") //۴
    .replaceAll("\u0665", "5") //٥
    .replaceAll("\u06F5", "5") //۵
    .replaceAll("\u0666", "6") //٦
    .replaceAll("\u06F6", "6") //۶
    .replaceAll("\u0667", "7") //٧
    .replaceAll("\u06F7", "7") //۷
    .replaceAll("\u0668", "8") //٨
    .replaceAll("\u06F8", "8") //۸
    .replaceAll("\u0669", "9") //٩
    .replaceAll("\u06F9", "9"); //۹;
}

export function notifyError(message) {
  toast(message, {
    type: "error"
  });
}

export const getRole = () =>
  localStorage.getItem("user")
    ? JSON.parse(localStorage.getItem("user")).role
    : null;
