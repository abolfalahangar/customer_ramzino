import Vue from "vue";
import "./socket"
const Auth = require("@/api/app/auth").default;
const Dashboard = require("@/api/services/dashboard").default;
const EarnMoney = require("@/api/services/earn-money").default;
const Wallets = require("@/api/services/wallets").default;
const MyBankAccounts = require("@/api/services/my-bank-accounts").default;
const DepositRecords = require("@/api/services/deposit-records").default;
const Announcements = require("@/api/services/announcements").default;
const OpenOrders = require("@/api/services/open-orders").default;
const OrdersRecords = require("@/api/services/orders-records").default;
const WithdrawRecords = require("@/api/services/withdraw-records").default;
const Transactions = require("@/api/services/transactions").default;

import http from "./http";

const factories = {
  auth: Auth(http),
  Dashboard: Dashboard(http),
  EarnMoney: EarnMoney(http),
  Wallets: Wallets(http),
  MyBankAccounts: MyBankAccounts(http),
  DepositRecords: DepositRecords(http),
  Announcements: Announcements(http),
  OpenOrders: OpenOrders(http),
  OrdersRecords: OrdersRecords(http),
  Transactions: Transactions(http),
  WithdrawRecords: WithdrawRecords(http)
};




Vue.prototype.$http = http;
Vue.prototype.$api = factories;
