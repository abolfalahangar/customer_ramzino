const DASHBOARD_BASE_ENDPOINT = "/api/dashboard";
const EARN_MONEY_BASE_ENDPOINT = "/api/earn-money";
const ANNOUNCEMENTS_BASE_ENDPOINT = "/api/announcements";
const DEPOSIT_RECORDS_BASE_ENDPOINT = "/api/deposit-records";
const MY_BANK_ACCOUNTS_BASE_ENDPOINT = "/api/my-bank-accounts";
const OPEN_ORDERS_BASE_ENDPOINT = "/api/open-orders";
const ORDERS_RECORDS_BASE_ENDPOINT = "/api/orders-records";
const TRANSACTIONS_BASE_ENDPOINT = "/api/transactions";
const WALLETS_BASE_ENDPOINT = "/api/wallets";
const WITHDRAW_RECORDS_BASE_ENDPOINT = "/api/withdraw-records";

const WITHDRAW_RECORDS_ENDPOINTS = {
  allWithdrawRecords: ""
};

const WALLETS_ENDPOINTS = {
  allWallets: ""
};

const TRANSACTIONS_ENDPOINTS = {
  allTransactions: ""
};

const ORDERS_RECORDS_ENDPOINTS = {
  ordersRecords: ""
};

const OPEN_ORDERS_ENDPOINTS = {
  openOrders: ""
};
const EARN_MONEY_ENDPOINTS = {
  myReferralCode: "/my-referral-code"
};
const ANNOUNCEMENTS_ENDPOINTS = {
  announcements: ""
};

const DEPOSIT_RECORDS_ENDPOINTS = {
  depositRecords: ""
};

const DASHBOARD_ENDPOINTS = {
  allMarkets: "/markets",
  marketIRT: "/markets/irt",
  marketUSDT: "/markets/usdt",
  recentTrades: "/recent-trades",
  activeOrdersSell: "/active-orders-sell",
  activeOrdersBuy: "/active-orders-buy",
  openOrders: "/open-orders",
  cancelOrders: "/cancel-orders",
  doneOrders: "/done-orders"
};

const MY_BANK_ACCOUNTS_ENDPOINTS = {
  myBankAccounts: ""
};

export {
  DASHBOARD_ENDPOINTS,
  DASHBOARD_BASE_ENDPOINT,
  EARN_MONEY_BASE_ENDPOINT,
  EARN_MONEY_ENDPOINTS,
  ANNOUNCEMENTS_ENDPOINTS,
  ANNOUNCEMENTS_BASE_ENDPOINT,
  DEPOSIT_RECORDS_BASE_ENDPOINT,
  DEPOSIT_RECORDS_ENDPOINTS,
  MY_BANK_ACCOUNTS_BASE_ENDPOINT,
  MY_BANK_ACCOUNTS_ENDPOINTS,
  OPEN_ORDERS_BASE_ENDPOINT,
  OPEN_ORDERS_ENDPOINTS,
  ORDERS_RECORDS_ENDPOINTS,
  ORDERS_RECORDS_BASE_ENDPOINT,
  TRANSACTIONS_ENDPOINTS,
  TRANSACTIONS_BASE_ENDPOINT,
  WALLETS_BASE_ENDPOINT,
  WALLETS_ENDPOINTS,
  WITHDRAW_RECORDS_ENDPOINTS,
  WITHDRAW_RECORDS_BASE_ENDPOINT
};
