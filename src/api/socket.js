
import Vue from 'vue';
import { HttpTransportType, HubConnectionBuilder } from "@microsoft/signalr";

const connection = new HubConnectionBuilder()
.withUrl(`${process.env.VUE_APP_BASE_URL}/ramzinosocket`, {
  skipNegotiation: true,
  transport: HttpTransportType.WebSockets,
  // headers:{
  //   Authorization:`${user.email}`
  // },
  //accessTokenFactory : ()=> localStorage.getItem("token")
})
.withAutomaticReconnect()
.build();

//`${process.env.VUE_APP_BASE_URL}/ramzinosocket`
// if(store.getters["user/user_is_authenticated"]){


// }
// connection.start().then( () =>{
//   console.log("socket connected");
// });



Vue.prototype.$socket = connection;