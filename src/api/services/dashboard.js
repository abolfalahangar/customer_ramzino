
const constants = require("../constants");

export default http => ({
  getAllMarket() {
    return http
      .get(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.allMarkets}`)
      .then(
        response => Promise.resolve(response),
        error => {
          console.log("error code", error?.code);
          console.log("error stack", error?.stack);
          console.log("error message", error?.message);
          console.log(
            "error?.response?.data?.error",
            error?.response?.data?.error
          );
          return Promise.reject(error);
        }
      );
  },

  getMarketIRT() {
    return http
      .get(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.marketIRT}`)
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  },

  getMarketUSDT() {
    return http
      .get(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.marketUSDT}`)
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  },

  getRecentTrades() {
    return http
      .get(
        `${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.recentTrades}`
      )
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  },

  getActiveOrdersSell() {
    return http
      .get(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.activeOrdersSell}`)
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  },
  getActiveOrdersBuy() {
    return http
      .get(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.activeOrdersBuy}`)
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  },
  getOpenOrders() {
    return http
      .get(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.openOrders}`)
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  },
  getCancelOrders() {
    return http
      .get(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.cancelOrders}`)
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  },
  getDoneOrders() {
    return http
      .get(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.doneOrders}`)
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  }
});
