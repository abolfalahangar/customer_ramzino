const constants = require("../constants");

export default http => ({
  getDepositRecords() {
    return http
      .get(
        `${constants.DEPOSIT_RECORDS_BASE_ENDPOINT}${constants.DEPOSIT_RECORDS_ENDPOINTS.depositRecords}`
      )
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  }
});
