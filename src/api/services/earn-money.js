const constants = require("../constants");
export default http => ({
  getMyReferralCode() {
    return http
      .get(`${constants.EARN_MONEY_BASE_ENDPOINT}${constants.EARN_MONEY_ENDPOINTS.myReferralCode}`)
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  }
});
