const constants = require("../constants");

export default http => ({
  getAllWithdrawRecords() {
    return http
      .get(
        `${constants.WITHDRAW_RECORDS_BASE_ENDPOINT}${constants.WITHDRAW_RECORDS_ENDPOINTS.allWithdrawRecords}`
      )
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  }
});
