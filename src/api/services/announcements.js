const constants = require("../constants");

export default http => ({
  getAnnouncements() {
    return http
      .get(
        `${constants.ANNOUNCEMENTS_BASE_ENDPOINT}${constants.ANNOUNCEMENTS_ENDPOINTS.announcements}`
      )
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  }
});
