const constants = require("../constants");

export default http => ({
  getOpenOrders() {
    return http
      .get(`${constants.OPEN_ORDERS_BASE_ENDPOINT}${constants.OPEN_ORDERS_ENDPOINTS.openOrders}`)
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  },
  jsonPlaceholder() {
    return http.get("http://jsonplaceholder.typicode.com/posts/2").then(
      response => Promise.resolve(response),
      error => Promise.reject(error)
    );
  }
});
