const constants = require("../constants");

export default http => ({
  getAllWallets() {
    return http
      .get(`${constants.WALLETS_BASE_ENDPOINT}${constants.WALLETS_ENDPOINTS.allWallets}`)
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  }
});
