const constants = require("../constants");

export default http => ({
  getTransactions() {
    return http
      .get(
        `${constants.TRANSACTIONS_BASE_ENDPOINT}${constants.TRANSACTIONS_ENDPOINTS.allTransactions}`
      )
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  }
});
