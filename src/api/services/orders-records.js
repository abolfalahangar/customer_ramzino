const constants = require("../constants");

export default http => ({
  getOrdersRecords() {
    return http
      .get(
        `${constants.ORDERS_RECORDS_BASE_ENDPOINT}${constants.ORDERS_RECORDS_ENDPOINTS.ordersRecords}`
      )
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  }
});
