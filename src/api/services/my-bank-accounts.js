const constants = require("../constants");

export default http => ({
  getMyBankAccounts() {
    return http
      .get(
        `${constants.MY_BANK_ACCOUNTS_BASE_ENDPOINT}${constants.MY_BANK_ACCOUNTS_ENDPOINTS.myBankAccounts}`
      )
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)
      );
  }
});
