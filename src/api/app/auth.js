export default http => ({
  forgotPassword(email) {
    return http.post("/auth/password/forgot", { email });
  },

  login(email, password) {
    return http.post("/auth/login", { email, password });
  },

  logout() {
    return http.get("/auth/logout");
  },

  register(payload) {
    return http.post("/auth/register", payload);
  },

  resetPassword(password, passwordConfirmation, resetToken) {
    return http.post("/auth/password/reset", {
      password: password,
      password_confirmation: passwordConfirmation,
      token: resetToken
    });
  }
});
