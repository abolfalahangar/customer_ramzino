import NProgress from "nprogress";
NProgress.configure({ speed: 900, trickleSpeed: 200, showSpinner: false });

export default NProgress;
