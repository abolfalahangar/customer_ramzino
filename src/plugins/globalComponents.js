import Vue from "vue";

import Snackbar from "@/components/Snackbar.vue";
import VueSlider from "vue-slider-component";
import "vue-slider-component/theme/default.css";
import LoadScript from 'vue-plugin-load-script';

Vue.use(LoadScript);

Vue.component("VueSlider", VueSlider);
Vue.component("snackbar", Snackbar);
