// Imports
import Vue from "vue";
import Router from "vue-router";
import NProgress from "@/plugins/nprogress";
import { MustAuth, NotAuth } from "@/middlewares/auth";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,

  scrollBehavior: (to, from, savedPosition) => {
    if (to.hash) return { selector: to.hash, behavior: "smooth" };
    if (savedPosition) return savedPosition;
    return { x: window.pageXOffset, y: window.pageYOffset, behavior: "smooth" };
  },
  routes: [
    {
      path: "",
      component: () =>
        import(
          /* webpackChunkName: "default-[request]" */ "@/layouts/default/Index.vue"
        ),
      children: [
        {
          path: "",
          redirect: "/dashboard/BTCTOMAN",
          meta: {
            rule: "editor",
            requiresAuth: true,
            animation: true
          }
        },
        {
          path: "/dashboard",
          redirect: "/dashboard/BTCTOMAN",
          meta: {
            rule: "editor",
            requiresAuth: true,
            animation: true
          }
        },
        {
          path: "/dashboard/:currency",
          name: "داشبورد",
          component: () =>
            import(
              /* webpackChunkName: "dashboard" */ "@/views/dashboard/Dashboard.vue"
            ),

          meta: {
            rule: "editor",
            requiresAuth: true,
            animation: false
          }
          // beforeEnter: (to, from, next) => {

          // }
        },
        {
          path: "/announcements",
          name: `اعلانات`,
          component: () =>
            import(
              /* webpackChunkName: "announcements" */ "@/views/announcements/Announcements.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/deposit-records",
          name: "سوابق واریز",
          component: () =>
            import(
              /* webpackChunkName: "deposit-records" */ "@/views/deposit-records/DepositRecords.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },

        {
          path: "/withdraw-records",
          name: "سوابق برداشت",
          component: () =>
            import(
              /* webpackChunkName: "withdraw-records" */ "@/views/withdraw-records/WithdrawRecords.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/my-bank-account",
          name: "حساب های بانکی من",
          component: () =>
            import(
              /* webpackChunkName: "my-bank-account" */ "@/views/my-bank-account/MyBankAccount.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/open-orders",
          name: "سفارشات باز",
          component: () =>
            import(
              /* webpackChunkName: "open-orders" */ "@/views/open-orders/OpenOrders.vue"
            ),

          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/orders-records",
          name: "سوابق سفارشات",
          component: () =>
            import(
              /* webpackChunkName: "orders-records" */ "@/views/orders-records/OrdersRecords.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/ticket-records",
          name: "سوابق تیکت ها",
          component: () =>
            import(
              /* webpackChunkName: "ticket-records" */ "@/views/ticket-records/TicketRecords.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },

        {
          path: "/trade-history",
          name: "تاریخچه معماملات",
          component: () =>
            import(
              /* webpackChunkName: "trade-history" */ "@/views/trade-history/TradeHistory.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },

        {
          path: "/transactions",
          name: "تراکنش ها",
          component: () =>
            import(
              /* webpackChunkName: "transactions" */ "@/views/transactions/Transactions.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/wallets",
          name: "کیف پول",
          component: () =>
            import(
              /* webpackChunkName: "wallet" */ "@/views/wallets/Wallets.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/earn-money",
          name: "کسب درآمد",
          component: () =>
            import(
              /* webpackChunkName: "earn-money" */ "@/views/earn-money/EarnMoney.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/security-settings",
          name: "تنظیمات امنیتی",
          component: () =>
            import(
              /* webpackChunkName: "security-settings" */ "@/views/security-settings/SecuritySettings.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/tickets",
          name: "تیکت ها",
          component: () =>
            import(
              /* webpackChunkName: "tickets" */ "@/views/tickets/Tickets.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/upgrade-level",
          name: "ارتقای سطح کاربری",
          component: () =>
            import(
              /* webpackChunkName: "upgrade-level" */ "@/views/upgrade-level/Upgrade.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/authentication",
          name: "احراز هویت",
          component: () =>
            import(
              /* webpackChunkName: "authentication" */ "@/views/upgrade-level/Authentication.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/deposite-and-withdraw",
          name: "deposite-and-withdraw",
          component: () =>
            import(
              /* webpackChunkName: "deposite-and-withdraw" */ "@/views/deposite-and-withdraw/d-w.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/withdraw/:symbol",
          name: "برداشت",
          component: () =>
            import(
              /* webpackChunkName: "withdraw" */ "@/views/deposite-and-withdraw/Withdraw.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/deposite/:symbol",
          name: "واریز",
          component: () =>
            import(
              /* webpackChunkName: "deposite" */ "@/views/deposite-and-withdraw/Deposite.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        // {
        //   path: "/fake",
        //   name: "fake",
        //   component: () =>
        //     import(
        //       /* webpackChunkName: "fake" */ "@/views/dashboard/fake.vue"
        //     ),
        //   meta: {
        //     rule: "editor",
        //     requiresAuth: true
        //   }
        // },
        // {
        //   path: "/test-captcha",
        //   name: "test-captcha",
        //   component: () =>
        //     import(
        //       /* webpackChunkName: "test-captcha" */ "@/views/auth-pages/testCaptcha.vue"
        //     ),
        //   meta: {
        //     rule: "editor",
        //     requiresAuth: true
        //   }
        // },
        
      ]
    },
    {
      path: "",
      component: () =>
        import(
          /* webpackChunkName: "blank-[request]" */ "@/layouts/blank/Index.vue"
        ),
      children: [
        {
          path: "/login",
          name: "ورود",
          component: () =>
            import(
              /* webpackChunkName: "login" */ "@/views/auth-pages/Login.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: false
          }
        },
        {
          path: "/register",
          name: "ثبت نام",
          component: () =>
            import(
              /* webpackChunkName: "register" */ "@/views/auth-pages/Register.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: false
          }
        },
        {
          path: "/maintenance",
          name: "نگهداری",
          component: () =>
            import(
              /* webpackChunkName: "maintenance" */ "@/views/utility-pages/Maintenance.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: false
          }
        },

        {
          path: "/error-404",
          name: "صفحه مورد نظر پیدا نشد",
          component: () =>
            import(
              /* webpackChunkName: "error-404" */ "@/views/error-pages/Error404.vue"
            ),
          meta: {
            requiresAuth: false
          }
        }
      ]
    },

    {
      path: "*",
      redirect: "/error-404"
    }
  ]
});

router.beforeResolve((to, from, next) => {
  if (to.path) {
    NProgress.start();
  }

  if (to.matched.some(r => r.meta.requiresAuth == true)) {
    MustAuth(to, from, next, router.app);
  } else if (to.matched.some(r => r.meta.requiresAuth == false)) {
    NotAuth(to, from, next, router.app);
  } else {
    next();
  }
});

router.afterEach((to, from) => {
  NProgress.done();
});

export default router;
