import Vue from "vue"
import VueCompositionAPI from '@vue/composition-api'
import router from "./router"
import vuetify from "./plugins/vuetify"
import store from "./store"
import App from "./App.vue"

import "./plugins";
import { sync } from "vuex-router-sync";

import "./assets/scss/main.scss";

import "./fake-db/index.js";
import "@/api/api.js";
import "./registerServiceWorker";
import wb from "./registerServiceWorker";

Vue.prototype.$workbox = wb;

Vue.config.productionTip = false;



Vue.use(VueCompositionAPI)
// Vue.config.async = true;
// Vue.config.devtools = true;
// Vue.config.performance = process.env.NODE_ENV !== "production";

// Vue.prototype.$workbox = wb;

// Vue.config.errorHandler = (err, vm, info) => {
//   // err: error trace
//   // vm: component in which error occured
//   // info: Vue specific error information such as lifecycle hooks, events etc.

  //   // TODO: Perform any custom logic or log to server

// };

// const token = localStorage.getItem('user-token')
// if (token) {
//   axios.defaults.headers.common['Authorization'] = token
// }

sync(store, router);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
