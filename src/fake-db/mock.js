import http from "@/api/http";
const MockAdapter = require("axios-mock-adapter");
const mock = new MockAdapter(http); // This sets the mock adapter on the default instance

export default mock;
