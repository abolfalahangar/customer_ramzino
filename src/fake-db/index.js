import mock from "./mock";
import "./data/user";
import "./data/tables";
import "./data/dashboard";
import "./data/tickets";
import "./data/earn-money";

mock.onAny().passThrough(); // forwards the matched request over network
