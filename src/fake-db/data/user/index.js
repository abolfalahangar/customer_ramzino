import mock from "@/fake-db/mock.js";
import jwt from "jsonwebtoken";
const data = {
  users: [
    { uid: 34, name: "user1", password: "121212" },
    { uid: 35, name: "user2", password: "131313" },
    { uid: 36, name: "user3", password: "141414" },
    { uid: 37, name: "user4", password: "151515" },
    { uid: 38, name: "user5", password: "161616" },
    { uid: 39, name: "user6", password: "171717" }
  ]
};

const jwtConfig = {
  secret: "dd5f3089-40c3-403d-af14-d0c228b05cb4",
  expireTime: 8000
};

mock.onPost("/api/auth/login").reply(request => {
  const { name, password } = JSON.parse(request.data);
  let error = "Something went wrong";
  let user = data.users.find(u => u.name == name && u.password == password);
  if (user) {
    try {
      const accessToken = jwt.sign({ id: user.uid }, jwtConfig.secret, {
        expiresIn: jwtConfig.expireTime
      });

      const userData = Object.assign({}, user, { providerId: "jwt" });

      const response = {
        userData: userData,
        accessToken: accessToken
      };
      return [201, response];
    } catch (e) {
      error = e;
    }
  } else {
    error = "ایمیل یا رمز عبور صحیح نمی باشد";
    return [400, { error }];
  }
});
