import mock from "@/fake-db/mock.js";
const constants = require("@/api/constants");

const data = {
  myReferralCode: {
    headers: [
      {
        text: "کد معرفی",
        value: "referralCode",
        sortable: false,
        align: "center"
      },
      {
        text: "پورسانت من",
        align: "center",
        sortable: false,
        value: "myCommission"
      },
      {
        text: "پورسانت معرفی شده",
        align: "center",
        sortable: false,
        value: "IntroducedCommission"
      },
      {
        text: "تعداد معرفی شده",
        align: "center",
        sortable: false,
        value: "numberIntroduced"
      },
      {
        text: "عنوان",
        align: "center",
        sortable: false,
        value: "title"
      },
      {
        text: "تاریخ",
        align: "center",
        sortable: false,
        value: "registrationDate"
      }
    ],
    myReferralCodes: [
      {
        referralCode: "38811091",
        myCommission: "20%",
        IntroducedCommission: "10%",
        numberIntroduced: "4",
        title: "علی",
        registrationDate: "15:43 1399/7/8"
      },
      {
        referralCode: "81109211",
        myCommission: "30%",
        IntroducedCommission: "0%",
        numberIntroduced: "3",
        title: "رضا",
        registrationDate: "12:51 1399/2/4"
      },
      {
        referralCode: "41002244",
        myCommission: "15%",
        IntroducedCommission: "15%",
        numberIntroduced: "2",
        title: "جعفر",
        registrationDate: "20:00 1399/5/7"
      }
    ]
  }
};

mock
  .onGet(`${constants.EARN_MONEY_BASE_ENDPOINT}${constants.EARN_MONEY_ENDPOINTS.myReferralCode}`)
  .reply(() => {
    return [200, data.myReferralCode];
  });
