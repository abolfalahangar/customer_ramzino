import mock from "@/fake-db/mock.js";
const constants = require("@/api/constants");

const data = {
  wallet: {
    headers: [
      {
        text: "ارز",
        value: "currency",
        sortable: false,
        align: "right"
      },
      {
        text: "موجودی",
        align: "center",
        sortable: false,
        value: "stock"
      },
      // { text: "آدرس", value: "address", sortable: false, align: "center" },
      { text: "", value: "actions", sortable: false, align: "left" }
    ],
    wallets: [
      {
        id: 1,
        currency: "تومان ایران",
        logo: "rls.svg",
        stock: "120,000,000 IRT",
        symbol: "IRT"
      },
      {
        id: 2,
        currency: "بیت کوین (BTC)",
        stock: "0.04576 BTC",
        logo: "bitcoin.png",
        symbol: "BTC"
      },
      {
        id: 3,
        currency: "دوح کوین (DOGE)",
        logo: "dogecoin.png",
        stock: "893 DOGE",
        symbol: "DOGE"
      },
      {
        id: 4,
        currency: "لایت کوین (LTC)",
        logo: "litecoin.png",
        stock: "12.45 LTC",
        symbol: "LTC"
      },
      {
        id: 5,
        currency: "دش (DASH)",
        logo: "dash.png",
        stock: "9.878 DASH",
        symbol: "DASH"
      }
    ]
  },
  transaction: {
    headers: [
      { text: "ارز", value: "currency", sortable: false, align: "center" },
      {
        text: "مقدار",
        align: "center",
        sortable: false,
        value: "amount"
      },
      {
        text: "قیمت",
        align: "center",
        sortable: false,
        value: "price"
      },
      { text: "کارمزد", value: "fee", sortable: false, align: "center" },
      { text: "نوع", value: "type", sortable: false, align: "center" },

      {
        text: "شرح",
        value: "description",
        sortable: false,
        align: "center"
      },
      {
        text: "تاریخ ثبت",
        value: "registrationDate",
        sortable: false,
        align: "center"
      }
    ],
    transactions: [
      {
        currency: "BTC",
        amount: "0.8",
        price: "IRT 8,410,454,589",
        fee: "BTC 0.00167",
        type: "ترید",
        description: "بابت سفارش 3319# (خرید BTC-IRT)",
        registrationDate: "15:43 1399/7/8"
      },
      {
        currency: "IRT",
        amount: "IRT 240,000,000",
        price: "IRT 210,000,000",
        fee: "IRT 200,000",
        type: "ترید",
        description: "بابت سفارش 3319# (خرید BTC-IRT)",
        registrationDate: "12:51 1399/2/4"
      },
      {
        currency: "BTC",
        amount: "0.6 BTC",
        price: "6,374,239,578 IRT",
        fee: "0.00142 BTC",
        type: "برداشت",
        description: "برداشت 3871#",
        registrationDate: "20:00 1399/5/7"
      },
      {
        currency: "IRT",
        amount: "50,000,000 IRT",
        price: "IRT 50,000,000",
        fee: "0",
        type: "واریز",
        description: "واریز 18805#",
        registrationDate: "16:23 1399/8/8"
      },
      {
        currency: "DASH",
        amount: "0.008",
        price: "IRT 3,000,000",
        fee: "0",
        type: "پورسانت",
        description: "بابت سفارش 8151# (خرید DASH-IRT)",
        registrationDate: "13:31 1399/8/9"
      },
      {
        currency: "BTC",
        amount: "0.6 BTC",
        price: "6,374,239,578 IRT",
        fee: "0.00142 BTC",
        type: "برداشت",
        description: "برداشت 3871#",
        registrationDate: "20:00 1399/5/7"
      },
      {
        currency: "IRT",
        amount: "240,000,000 IRT",
        price: "IRT 240,000,000",
        fee: "0",
        type: "واریز",
        description: "واریز 18805#",
        registrationDate: "16:23 1400/4/6"
      },
      {
        currency: "LTC",
        amount: "0.8",
        price: "IRT 30,264,300",
        fee: "0",
        type: "پورسانت",
        description: "بابت سفارش 8151# (خرید LTC-IRT)",
        registrationDate: "13:31 1399/8/9"
      },
      {
        currency: "BTC",
        amount: "0.6 BTC",
        price: "6,374,239,578 IRT",
        fee: "0.00142 BTC",
        type: "برداشت",
        description: "برداشت 3871#",
        registrationDate: "20:00 1399/5/7"
      },
      {
        currency: "IRT",
        amount: "50,000,000 IRT",
        price: "IRT 50,000,000",
        fee: "0",
        type: "واریز",
        description: "واریز 18805#",
        registrationDate: "16:23 1399/8/8"
      },
      {
        currency: "DASH",
        amount: "0.008",
        price: "IRT 3,000,000",
        fee: "0",
        type: "پورسانت",
        description: "بابت سفارش 8151# (خرید DASH-IRT)",
        registrationDate: "13:31 1399/8/9"
      },
      {
        currency: "BTC",
        amount: "0.6 BTC",
        price: "6,374,239,578 IRT",
        fee: "0.00142 BTC",
        type: "برداشت",
        description: "برداشت 3871#",
        registrationDate: "20:00 1399/5/7"
      },
      {
        currency: "IRT",
        amount: "240,000,000 IRT",
        price: "IRT 240,000,000",
        fee: "0",
        type: "واریز",
        description: "واریز 18805#",
        registrationDate: "16:23 1400/4/6"
      },
      {
        currency: "LTC",
        amount: "0.8",
        price: "IRT 30,264,300",
        fee: "0",
        type: "پورسانت",
        description: "بابت سفارش 8151# (خرید LTC-IRT)",
        registrationDate: "13:31 1399/8/9"
      }
    ]
  },
  ordersRecords: {
    headers: [
      { text: "بازار", value: "market", sortable: false, align: "center" },

      {
        text: "سمت",
        align: "center",
        sortable: false,
        value: "position"
      },
      { text: "مقدار", value: "amount", sortable: false, align: "center" },
      { text: "قیمت", value: "price", sortable: false, align: "center" },
      { text: "نوع", value: "type", sortable: false, align: "center" },
      { text: "وضعیت", value: "status", sortable: false, align: "center" },
      {
        text: "کارمزد",
        value: "fee",
        sortable: false,
        align: "center"
      },
      {
        text: "تاریخ ثبت",
        value: "registrationDate",
        sortable: false,
        align: "center"
      }
    ],
    ordersRecords: [
      {
        status: "پر شده",
        price: "IRT 880,254,536",
        position: "خرید",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        fee: "BTC 0.00086",
        registrationDate: "15:43 1400/3/3",
        amount: "BTC 0.09"
      },
      {
        status: "انصرافی",
        price: "640,484,276 IRT",
        position: "فروش",
        market: "BTC-IRT",
        type: "قیمت بازار",
        fee: "BTC 0.000234",
        registrationDate: "13:41 1400/5/4",
        amount: "BTC 0.0658"
      },
      {
        status: "انصرافی",
        price: "294,546,646 IRT",
        position: "فروش",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        fee: "BTC 0.00007",
        registrationDate: "08:31 1400/2/15",
        amount: "BTC 0.03"
      },
      {
        status: "پر شده",
        price: "7,84,000,000 IRT",
        position: "فروش",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        fee: "BTC 0.000943",
        registrationDate: "14:09 1400/1/26",
        amount: "BTC 0.8"
      },
      {
        status: "انصرافی",
        price: "1,176,618,492 IRT",
        position: "خرید",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        fee: "BTC 0.00007",
        registrationDate: "16:03 1400/5/21",
        amount: "BTC 0.12"
      },
      {
        status: "پر شده",
        price: "8,918,196,294 IRT",
        position: "فروش",
        market: "BTC-IRT",
        type: "قیمت بازار",
        fee: "BTC 0.001",
        registrationDate: "11:21 1400/5/27",
        amount: "BTC 0.91"
      },
      {
        status: "انصرافی",
        price: "3,950,646 IRT",
        position: "فروش",
        market: "DOGE-IRT",
        type: "قیمت دلخواه",
        fee: "DOGE 7",
        registrationDate: "00:01 1400/2/15",
        amount: "DOGE 79"
      },
      {
        status: "پر شده",
        price: "17,235,729 IRT",
        position: "فروش",
        market: "DOGE-IRT",
        type: "قیمت دلخواه",
        fee: "DOGE 43",
        registrationDate: "22:54 1400/1/26",
        amount: "DOGE 300"
      },
      {
        status: "انصرافی",
        price: "10,105,920 IRT",
        position: "خرید",
        market: "DOGE-IRT",
        type: "قیمت دلخواه",
        fee: "DOGE 21",
        registrationDate: "07:14 1400/5/21",
        amount: "DOGE 176"
      },
      {
        status: "پر شده",
        price: "11,484,401 IRT",
        position: "فروش",
        market: "DOGE-IRT",
        type: "قیمت بازار",
        fee: "DOGE 25",
        registrationDate: "09:51 1400/5/27",
        amount: "DOGE 200"
      },

      {
        status: "انصرافی",
        price: "72,434,646 IRT",
        position: "فروش",
        market: "LTC-IRT",
        type: "قیمت دلخواه",
        fee: "LTC 0.017",
        registrationDate: "10:11 1400/2/15",
        amount: "LTC 2"
      },
      {
        status: "پر شده",
        price: "244,165,597 IRT",
        position: "فروش",
        market: "LTC-IRT",
        type: "قیمت دلخواه",
        fee: "LTC 0.042",
        registrationDate: "15:43 1400/1/01",
        amount: "LTC 6.75"
      },
      {
        status: "انصرافی",
        price: "378,727,950 IRT",
        position: "خرید",
        market: "LTC-IRT",
        type: "قیمت دلخواه",
        fee: "LTC 0.069",
        registrationDate: "14:09 1400/2/30",
        amount: "LTC 10.47"
      },
      {
        status: "پر شده",
        price: "196,700,441 IRT",
        position: "فروش",
        market: "LTC-IRT",
        type: "قیمت بازار",
        fee: "LTC 0.029",
        registrationDate: "04:02 1400/5/29",
        amount: "LTC 5.43"
      },

      {
        status: "انصرافی",
        price: "27,397,326 IRT",
        position: "فروش",
        market: "DASH-IRT",
        type: "قیمت دلخواه",
        fee: "DASH 0.008",
        registrationDate: "09:39 1400/1/22",
        amount: "DASH 0.658"
      },
      {
        status: "پر شده",
        price: "74,142,739 IRT",
        position: "فروش",
        market: "DASH-IRT",
        type: "قیمت دلخواه",
        fee: "DASH 0.031",
        registrationDate: "11:00 1400/1/26",
        amount: "DASH 1.78"
      },
      {
        status: "انصرافی",
        price: "165,779,817 IRT",
        position: "خرید",
        market: "DASH-IRT",
        type: "قیمت دلخواه",
        fee: "DASH 0.083",
        registrationDate: "01:11 1400/3/23",
        amount: "DASH 3.98"
      },
      {
        status: "پر شده",
        price: "92,886,681 IRT",
        position: "فروش",
        market: "DASH-IRT",
        type: "قیمت بازار",
        fee: "DASH 0.051",
        registrationDate: "05:51 1400/5/29",
        amount: "DASH 2.23"
      }
    ]
  },
  openOrders: {
    headers: [
      { text: "بازار", value: "market", sortable: false, align: "center" },

      {
        text: "سمت",
        align: "center",
        sortable: false,
        value: "position"
      },
      { text: "مقدار", value: "amount", sortable: false, align: "center" },
      { text: "قیمت", value: "price", sortable: false, align: "center" },
      { text: "نوع", value: "type", sortable: false, align: "center" },
      { text: "پر شده", value: "filled", sortable: false, align: "center" },
      {
        text: "کارمزد",
        value: "fee",
        sortable: false,
        align: "center"
      },
      {
        text: "تاریخ ثبت",
        value: "registrationDate",
        sortable: false,
        align: "center"
      },
      { text: "", value: "actions", align: "center", sortable: false }
    ],
    openOrders: [
      {
        status: "تایید شده",
        price: "IRT 2,940,000,000",
        position: "خرید",
        market: "BTC-IRT",
        type: "بازار",
        filled: "80",
        fee: "BTC 0.000421",
        registrationDate: "14:21 1399/4/3",
        amount: "BTC 0.00389"
      },
      {
        status: "تایید شده",
        price: "3,780,000,000 IRT",
        position: "فروش",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "70",
        fee: "BTC 0.00061",
        registrationDate: "14:21 1399/7/30",
        amount: "BTC 0.0043"
      },
      {
        status: "تایید شده",
        price: "5,360,000,000 IRT",
        position: "خرید",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "80",
        fee: "BTC 0.00074",
        registrationDate: "14:21 1400/1/3",
        amount: "BTC 0.000518"
      },
      {
        status: "تایید شده",
        price: "5,070,000 IRT",
        position: "خرید",
        market: "DOGE-IRT",
        type: "قیمت دلخواه",
        filled: "50",
        fee: "DOGE 8",
        registrationDate: "14:21 1400/4/3",
        amount: "DOGE 100"
      },
      {
        status: "تایید شده",
        price: "7,818,580 IRT",
        position: "خرید",
        market: "DOGE-IRT",
        type: "بازار",
        filled: "70",
        fee: "13 DOGE",
        registrationDate: "19:46 1400/4/21",
        amount: "154 DOGE"
      },
      {
        status: "تایید شده",
        price: "2,538,500 IRT",
        position: "فروش",
        market: "DOGE-IRT",
        type: "قیمت دلخواه",
        filled: "40",
        fee: "DOGE 4",
        registrationDate: "17:43 1400/4/29",
        amount: "DOGE 50"
      },
      {
        status: "تایید شده",
        price: "3,000,000,000 IRT",
        position: "خرید",
        market: "DOGE-IRT",
        type: "قیمت دلخواه",
        filled: "75",
        fee: "DOGE 0.9",
        registrationDate: "18:56 1400/4/23",
        amount: "DOGE 15"
      },
      {
        status: "تایید شده",
        price: "140,253,728 IRT",
        position: "خرید",
        market: "LTC-IRT",
        type: "قیمت دلخواه",
        filled: "90",
        fee: "LTC 0.00314",
        registrationDate: "18:01 1399/12/3",
        amount: "LTC 4"
      },
      {
        status: "تایید شده",
        price: "70,114,106 IRT",
        position: "خرید",
        market: "LTC-IRT",
        type: "بازار",
        filled: "90",
        fee: "LTC 0.001",
        registrationDate: "14:49 1400/10/15",
        amount: "LTC 2"
      },
      {
        status: "تایید شده",
        price: "141,079,621 IRT",
        position: "خرید",
        market: "LTC-IRT",
        type: "قیمت دلخواه",
        filled: "60",
        fee: "LTC 0.00323",
        registrationDate: "01:23 1399/12/3",
        amount: "LTC 4"
      },
      {
        status: "تایید شده",
        price: "590,482,350 IRT",
        position: "خرید",
        market: "DASH-IRT",
        type: "بازار",
        filled: "65",
        fee: "DASH 0.0385",
        registrationDate: "13:55 1399/10/17",
        amount: "DASH 15"
      },
      {
        status: "تایید شده",
        price: "66,313,060 IRT",
        position: "فروش",
        market: "DASH-IRT",
        type: "قیمت دلخواه",
        filled: "90",
        fee: "DASH 0.007",
        registrationDate: "02:54 1399/9/18",
        amount: "DASH 1.676"
      },
      {
        status: "تایید شده",
        price: "80,386,2478 IRT",
        position: "خرید",
        market: "DASH-IRT",
        type: "قیمت دلخواه",
        filled: "75",
        fee: "DASH 0.0023",
        registrationDate: "17:37 1400/8/14",
        amount: "DASH 2.454"
      },

      {
        status: "تایید شده",
        price: "150,862,508 IRT",
        position: "فروش",
        market: "DASH-IRT",
        type: "بازار",
        filled: "65",
        fee: "DASH 0.0043",
        registrationDate: "20:42 1400/2/27",
        amount: "DASH 4"
      }
    ]
  },
  withdrawRecords: {
    headers: [
      { text: "ارز", value: "currency", sortable: false, align: "center" },
      {
        text: "مقدار",
        align: "center",
        sortable: false,
        value: "amount"
      },
      { text: "وضعیت", value: "status", sortable: false, align: "center" },
      {
        text: "تاریخ ثبت",
        value: "registrationDate",
        sortable: false,
        align: "center"
      },
      {
        text: "تاریخ برداشت",
        value: "withdrawDate",
        sortable: false,
        align: "center"
      },
      {
        text: "لینک تراکنش/کد رهگیری",
        value: "transactionLink",
        sortable: false,
        align: "center"
      }
    ],
    withdrawRecords: [
      {
        currency: "BTC",
        amount: "0.8",
        status: "پرداخت شده",
        registrationDate: "18:56 1400/4/3",
        withdrawDate: "12:23 1400/4/6",
        transactionLink: "18233"
      },
      {
        currency: "BTC",
        amount: "0.03",
        status: "پرداخت شده",
        registrationDate: "19:32 1400/4/3",
        withdrawDate: "22:11 1400/4/12",
        transactionLink: "18234"
      },
      {
        currency: "BTC",
        amount: "0.0032",
        status: "پرداخت شده",
        registrationDate: "14:32 1400/5/3",
        withdrawDate: "15:59 1400/5/6",
        transactionLink: "18235"
      },
      {
        currency: "IRT",
        amount: "10,000,000",
        status: "پرداخت نشده",
        registrationDate: "17:06 1400/1/4",
        withdrawDate: "13:45 1400/4/5",
        transactionLink: "18236"
      },
      {
        currency: "IRT",
        amount: "22,000,000",
        status: "پرداخت نشده",
        registrationDate: "11:06 1400/3/11",
        withdrawDate: "12:00 1400/4/5",
        transactionLink: "18237"
      },
      {
        currency: "IRT",
        amount: "34,000,000",
        status: "پرداخت نشده",
        registrationDate: "10:09 1400/1/4",
        withdrawDate: "02:54 1400/4/5",
        transactionLink: "18238"
      },
      {
        currency: "IRT",
        amount: "23,000,000",
        status: "پرداخت شده",
        registrationDate: "21:55 1400/1/4",
        withdrawDate: "02:45 1400/4/5",
        transactionLink: "18239"
      },
      {
        currency: "IRT",
        amount: "1,000,000",
        status: "پرداخت نشده",
        registrationDate: "17:44 1400/2/24",
        withdrawDate: "03:16 1400/2/25",
        transactionLink: "18240"
      },
      {
        currency: "LTC",
        amount: "2.43",
        status: "پرداخت شده",
        registrationDate: "11:06 1400/3/11",
        withdrawDate: "12:00 1400/4/5",
        transactionLink: "18241"
      },
      {
        currency: "LTC",
        amount: "3.5435",
        status: "پرداخت نشده",
        registrationDate: "10:09 1400/1/4",
        withdrawDate: "02:54 1400/4/5",
        transactionLink: "18242"
      },
      {
        currency: "LTC",
        amount: "1.989",
        status: "پرداخت نشده",
        registrationDate: "21:55 1400/1/4",
        withdrawDate: "02:45 1400/4/5",
        transactionLink: "18243"
      },
      {
        currency: "LTC",
        amount: "0.3423",
        status: "پرداخت شده",
        registrationDate: "17:44 1400/2/24",
        withdrawDate: "03:16 1400/2/25",
        transactionLink: "18244"
      },
      {
        currency: "DASH",
        amount: "8.32",
        status: "پرداخت نشده",
        registrationDate: "11:06 1400/3/11",
        withdrawDate: "12:00 1400/4/5",
        transactionLink: "18245"
      },
      {
        currency: "DASH",
        amount: "7",
        status: "پرداخت نشده",
        registrationDate: "10:09 1400/1/4",
        withdrawDate: "02:54 1400/4/5",
        transactionLink: "18246"
      },
      {
        currency: "DASH",
        amount: "12.435",
        status: "پرداخت نشده",
        registrationDate: "21:55 1400/1/4",
        withdrawDate: "02:45 1400/4/5",
        transactionLink: "18247"
      },
      {
        currency: "DASH",
        amount: "0.978",
        status: "پرداخت شده",
        registrationDate: "17:44 1400/2/26",
        withdrawDate: "03:16 1400/2/28",
        transactionLink: "18248"
      }
    ]
  },
  depositRecords: {
    headers: [
      { text: "ارز", value: "currency", sortable: false, align: "center" },
      {
        text: "مقدار",
        align: "center",
        sortable: false,
        value: "amount"
      },
      { text: "وضعیت", value: "status", sortable: false, align: "center" },
      {
        text: "تاریخ ثبت",
        value: "registrationDate",
        sortable: false,
        align: "center"
      },
      {
        text: "تاریخ واریز",
        value: "depositDate",
        sortable: false,
        align: "center"
      },
      {
        text: "لینک تراکنش/کد رهگیری",
        value: "transactionLink",
        sortable: false,
        align: "center"
      }
    ],
    depositRecords: [
      {
        currency: "BTC",
        amount: "0.7",
        status: "پرداخت شده",
        registrationDate: "14:21 1400/04/03",
        depositDate: "21:03 1400/02/26",
        transactionLink: "25479"
      },
      {
        currency: "IRT",
        amount: "20,000,000",
        status: "پرداخت نشده",
        registrationDate: "14:21 1400/4/3",
        depositDate: "12:17 1400/02/18",
        transactionLink: "25480"
      },
      {
        currency: "IRT",
        amount: "100,000,000",
        status: "پرداخت نشده",
        registrationDate: "14:21 1400/04/03",
        depositDate: "07:19 1400/01/02",
        transactionLink: "25481"
      },
      {
        currency: "LTC",
        amount: "2.7687",
        status: "پرداخت شده",
        registrationDate: "14:21 1400/02/01",
        depositDate: "14:24 1400/2/21",
        transactionLink: "25482"
      },
      {
        currency: "DOGE",
        amount: "312",
        status: "پرداخت نشده",
        registrationDate: "14:21 1400/4/13",
        depositDate: "13:28 1400/2/23",
        transactionLink: "25483"
      },
      {
        currency: "DASH",
        amount: "23.98",
        status: "پرداخت شده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "16:51 1399/2/26",
        transactionLink: "25484"
      },
      {
        currency: "IRT",
        amount: "86,000,000",
        status: "پرداخت نشده",
        registrationDate: "14:21 1400/4/14",
        depositDate: "12:25 1400/2/27",
        transactionLink: "25485"
      },
      {
        currency: "BTC",
        amount: "0.7",
        status: "پرداخت شده",
        registrationDate: "14:21 1400/4/17",
        depositDate: "21:03 1400/5/26",
        transactionLink: "25486"
      },
      {
        currency: "IRT",
        amount: "20,000,000",
        status: "پرداخت نشده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "12:25 1399/2/18",
        transactionLink: "25487"
      },
      {
        currency: "IRT",
        amount: "100,000,000",
        status: "پرداخت نشده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "12:25 1399/2/18",
        transactionLink: "25488"
      },
      {
        currency: "LTC",
        amount: "2.7687",
        status: "پرداخت شده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "12:25 1399/2/18",
        transactionLink: "25489"
      },
      {
        currency: "DOGE",
        amount: "312",
        status: "پرداخت نشده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "12:25 1399/2/18",
        transactionLink: "25490"
      },
      {
        currency: "DASH",
        amount: "23.98",
        status: "پرداخت شده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "12:25 1399/2/18",
        transactionLink: "25491"
      },
      {
        currency: "IRT",
        amount: "86,000,000",
        status: "پرداخت نشده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "12:25 1399/2/18",
        transactionLink: "25492"
      },
      {
        currency: "BTC",
        amount: "0.7",
        status: "پرداخت شده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "21:03 1398/6/26",
        transactionLink: "25493"
      },
      {
        currency: "IRT",
        amount: "20,000,000",
        status: "پرداخت نشده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "12:25 1399/2/18",
        transactionLink: "25494"
      },
      {
        currency: "IRT",
        amount: "100,000,000",
        status: "پرداخت شده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "02:01 1399/2/18",
        transactionLink: "25495"
      },
      {
        currency: "LTC",
        amount: "2.7687",
        status: "پرداخت نشده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "03:55 1399/2/18",
        transactionLink: "25496"
      },
      {
        currency: "DOGE",
        amount: "312",
        status: "پرداخت شده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "09:58 1399/2/18",
        transactionLink: "25497"
      },
      {
        currency: "DASH",
        amount: "23.98",
        status: "پرداخت نشده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "10:11 1399/2/18",
        transactionLink: "25498"
      },
      {
        currency: "IRT",
        amount: "86,000,000",
        status: "پرداخت شده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "10:25 1399/2/18",
        transactionLink: "25499"
      },
      {
        currency: "BTC",
        amount: "0.7",
        status: "پرداخت شده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "21:03 1399/6/26",
        transactionLink: "25500"
      },
      {
        currency: "IRT",
        amount: "20,000,000",
        status: "پرداخت نشده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "12:25 1399/2/18",
        transactionLink: "25501"
      },
      {
        currency: "IRT",
        amount: "100,000,000",
        status: "پرداخت شده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "04:25 1399/6/18",
        transactionLink: "25502"
      },
      {
        currency: "LTC",
        amount: "2.7687",
        status: "پرداخت نشده",
        registrationDate: "14:21 1399/4/3",
        depositDate: "11:05 1399/2/18",
        transactionLink: "25503"
      },
      {
        currency: "DOGE",
        amount: "312",
        status: "پرداخت شده",
        registrationDate: "14:21 1400/3/3",
        depositDate: "12:22 1400/3/19",
        transactionLink: "25504"
      },
      {
        currency: "DASH",
        amount: "23.98",
        status: "پرداخت نشده",
        registrationDate: "14:21 1400/4/3",
        depositDate: "02:25 1400/1/26",
        transactionLink: "25505"
      },
      {
        currency: "IRT",
        amount: "86,000,000",
        status: "پرداخت شده",
        registrationDate: "14:21 1400/4/3",
        depositDate: "11:45 1400/2/18",
        transactionLink: "25506"
      }
    ]
  },
  MyBankAccounts: {
    headers: [
      { text: "بانک", value: "bank", sortable: false, align: "center" },

      {
        text: "شماره کارت",
        align: "center",
        sortable: false,
        value: "cartNumber"
      },
      {
        text: "شماره شبا",
        value: "shabanumber",
        sortable: false,
        align: "center"
      },
      {
        text: "شماره حساب",
        value: "accountNumber",
        sortable: false,
        align: "center"
      },
      { text: "وضعیت", value: "status", sortable: false, align: "center" }
    ],
    MyBankAccountOptions: [
      {
        status: "تایید شده",
        accountNumber: "8681362158",
        cartNumber: "6037_3122_5318_7543",
        bank: "ملی",
        shabanumber: "IR 8681365800000000000"
      },
      {
        status: "تایید شده",
        accountNumber: "--------------",
        cartNumber: "6104_3373_6498_1588",
        bank: "ملت",
        shabanumber: "IR 3401365800000000000"
      },
      {
        status: "تایید نشده",
        accountNumber: "8681362158",
        cartNumber: "6087_9555_4821_8730",
        bank: "تجارت",
        shabanumber: "IR 8681365800000000000"
      },
      {
        status: "تایید شده",
        accountNumber: "0262443463",
        cartNumber: "5837_7634_9118_3425",
        bank: "رفاه",
        shabanumber: "IR 8681365800000000000"
      }
    ]
  },
  announcements: {
    headers: [
      { text: "شرح", value: "description", sortable: false, align: "center" },
      {
        text: "تاریخ",
        align: "center",
        sortable: false,
        value: "date"
      }
    ],
    announcements: [
      {
        description: "ورود موفق",
        date: "15:12 1399/2/3"
      },
      {
        description: "خروج",
        date: "21:01 1399/5/9"
      },
      {
        description: "ثبت سفارش",
        date: "14:52 1399/8/8"
      },
      {
        description: "تکمیل سفارش",
        date: "08:05 1399/5/9"
      }
    ]
  }
};

mock
  .onGet(`${constants.WALLETS_BASE_ENDPOINT}${constants.WALLETS_ENDPOINTS.allWallets}`)
  .reply(() => {
    return [200, data.wallet];
  });

mock
  .onGet(
    `${constants.TRANSACTIONS_BASE_ENDPOINT}${constants.TRANSACTIONS_ENDPOINTS.allTransactions}`
  )
  .reply(() => {
    return [200, data.transaction];
  });

mock
  .onGet(
    `${constants.ORDERS_RECORDS_BASE_ENDPOINT}${constants.ORDERS_RECORDS_ENDPOINTS.ordersRecords}`
  )
  .reply(() => {
    return [200, data.ordersRecords];
  });

mock
  .onGet(`${constants.OPEN_ORDERS_BASE_ENDPOINT}${constants.OPEN_ORDERS_ENDPOINTS.openOrders}`)
  .reply(() => {
    return [200, data.openOrders];
  });

mock
  .onGet(
    `${constants.WITHDRAW_RECORDS_BASE_ENDPOINT}${constants.WITHDRAW_RECORDS_ENDPOINTS.allWithdrawRecords}`
  )
  .reply(() => {
    return [200, data.withdrawRecords];
  });
mock
  .onGet(
    `${constants.DEPOSIT_RECORDS_BASE_ENDPOINT}${constants.DEPOSIT_RECORDS_ENDPOINTS.depositRecords}`
  )
  .reply(() => {
    return [200, data.depositRecords];
  });

mock
  .onGet(
    `${constants.MY_BANK_ACCOUNTS_BASE_ENDPOINT}${constants.MY_BANK_ACCOUNTS_ENDPOINTS.myBankAccounts}`
  )
  .reply(() => {
    return [200, data.MyBankAccounts];
  });

mock
  .onGet(
    `${constants.ANNOUNCEMENTS_BASE_ENDPOINT}${constants.ANNOUNCEMENTS_ENDPOINTS.announcements}`
  )
  .reply(() => {
    return [200, data.announcements];
  });

export default data;
