import mock from "@/fake-db/mock.js";

const constants = require("@/api/constants");

const dashboard = {
  openOrders: {
    openOrders: [
      {
        id: 1,
        status: "تایید شده",
        price: "1,593,000,000 IRT",
        position: "خرید",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "--------",
        allAmount: "1,593,000,000 IRT",
        registrationDate: "11:43 1400/3/3",
        actions: "--------"
      },
      {
        id: 2,
        status: "تایید شده",
        price: "492,000,000 IRT",
        position: "خرید",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "--------",
        allAmount: "492,000,000 IRT",
        registrationDate: "03:06 1400/2/13",
        actions: "--------"
      },
      {
        id: 3,
        status: "تایید شده",
        price: "1,270,000,000 IRT",
        position: "خرید",
        market: "BTC-IRT",
        type: "قیمت بازار",
        filled: "--------",
        allAmount: "1,270,000,000 IRT",
        registrationDate: "09:47 1400/2/31",
        actions: "--------"
      },
      {
        id: 4,
        status: "تایید شده",
        price: "60,000,000 IRT",
        position: "خرید",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "--------",
        allAmount: "60,000,000 IRT",
        registrationDate: "21:46 1400/5/8",
        actions: "--------"
      },
      {
        id: 5,
        status: "تایید شده",
        price: "970,000,000 IRT",
        position: "خرید",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "--------",
        allAmount: "970,000,000 IRT",
        registrationDate: "18:19 1400/5/25",
        actions: "--------"
      },
      {
        id: 6,
        status: "تایید شده",
        price: "750,000,000 IRT",
        position: "خرید",
        market: "BTC-IRT",
        type: "قیمت بازار",
        filled: "--------",
        allAmount: "750,000,000 IRT",
        registrationDate: "13:28 1400/5/3",
        actions: "--------"
      },
      {
        id: 7,
        status: "تایید شده",
        price: "1,679,000,000 IRT",
        position: "خرید",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "--------",
        allAmount: "1,679,000,000 IRT",
        registrationDate: "14:28 1400/2/23",
        actions: "--------"
      },
      {
        id: 8,
        status: "تایید شده",
        price: "1,890,000,000 IRT",
        position: "خرید",
        market: "BTC-IRT",
        type: "قیمت بازار",
        filled: "--------",
        allAmount: "1,890,000,000 IRT",
        registrationDate: "18:31 1400/4/6",
        actions: "--------"
      },
      {
        id: 9,
        status: "تایید شده",
        price: "2,789,000,000 IRT",
        position: "خرید",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "--------",
        allAmount: "2,789,000,000 IRT",
        registrationDate: "11:28 1400/1/30",
        actions: "--------"
      }
    ],
    headersOpen: [
      {
        text: "#",
        align: "center",
        sortable: false,
        value: "id"
      },
      {
        text: "تاریخ",
        sortable: false,
        value: "registrationDate",
        align: "center"
      },
      { text: "بازار", sortable: false, value: "market", align: "center" },
      { text: "وضعیت", sortable: false, value: "status", align: "center" },
      { text: "نوع", sortable: false, value: "type", align: "center" },
      { text: "قیمت", sortable: false, value: "price", align: "center" },
      {
        text: "تکمیل سفارش",
        sortable: false,
        value: "filled",
        align: "center"
      },
      {
        text: "مبلغ کل",
        sortable: false,
        value: "allAmount",
        align: "center"
      },
      { text: "اقدامات", sortable: false, value: "actions", align: "center" }
    ]
  },
  cancelOrders: {
    cancelOrders: [
      {
        id: 1,
        status: "تایید شده",
        price: "IRT 1,270,000,000",
        amount: "0.0083",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "75%",
        allAmount: "IRT 1,270,000,000",
        registrationDate: "08:01 1400/2/3",
        customer: "ابوالفضل آهنگر"
      },
      {
        id: 2,
        status: "تایید شده",
        price: "1,300,000,000 IRT",
        amount: "0.0091",
        market: "BTC-IRT",
        type: "قیمت بازار",
        filled: "80%",
        allAmount: "1,300,000,000 IRT",
        registrationDate: "13:11 1400/5/12",
        customer: "ابوالفضل آهنگر"
      },
      {
        id: 3,
        status: "تایید شده",
        price: "300,000,000 IRT",
        amount: "0.00079",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "40%",
        allAmount: "290,000,000 IRT",
        registrationDate: "11:54 1400/4/2",
        customer: "ابوالفضل آهنگر"
      },
      {
        id: 4,
        status: "تایید شده",
        price: "2,709,000,000 IRT",
        amount: "0.0206",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "70%",
        allAmount: "2,600,000,000 IRT",
        registrationDate: "22:11 1400/2/25",
        customer: "ابوالفضل آهنگر"
      },
      {
        id: 5,
        status: "تایید شده",
        price: "1,800,000,000 IRT",
        amount: "0.0101",
        market: "BTC-IRT",
        type: "قیمت بازار",
        filled: "90%",
        allAmount: "1,800,000,000 IRT",
        registrationDate: "13:11 1400/1/16",
        customer: "ابوالفضل آهنگر"
      }
    ],
    headersCancel: [
      {
        text: "#",
        align: "center",
        sortable: false,
        value: "id"
      },
      {
        text: "تاریخ",
        value: "registrationDate",
        sortable: false,
        align: "center"
      },
      { text: "بازار", value: "market", sortable: false, align: "center" },
      {
        text: "خریدار/فروشنده",
        sortable: false,
        value: "customer",
        align: "center"
      },
      { text: "نوع", value: "type", sortable: false, align: "center" },
      { text: "قیمت", value: "price", sortable: false, align: "center" },
      { text: "مقدار", value: "amount", sortable: false, align: "center" },
      { text: "پر شده", value: "filled", sortable: false, align: "center" },
      {
        text: "مبلغ کل",
        value: "allAmount",
        sortable: false,
        align: "center"
      }
    ]
  },
  doneOrders: {
    doneOrders: [
      {
        id: 1,
        status: "تایید شده",
        price: "1,270,000,000 IRT",
        amount: "0.00727",
        market: "BTC-IRT",
        type: "قیمت بازار",
        filled: "75%",
        allAmount: "1,270,000,000 IRT",
        registrationDate: "14:21 1400/4/3",
        customer: "ابوالفضل آهنگر"
      },
      {
        id: 2,
        status: "تایید شده",
        price: "1,798,000,000 IRT",
        amount: "0.00984",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "80%",
        allAmount: "1,798,000,000 IRT",
        registrationDate: "07:13 1400/5/22",
        customer: "ابوالفضل آهنگر"
      },
      {
        id: 3,
        status: "تایید شده",
        price: "270,000,000 IRT",
        amount: "0.00087",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "75%",
        allAmount: "270,000,000 IRT",
        registrationDate: "12:41 1400/4/3",
        customer: "ابوالفضل آهنگر"
      },
      {
        id: 4,
        status: "تایید شده",
        price: "1,280,000,000 IRT",
        amount: "0.00734",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "80%",
        allAmount: "1,280,000,000 IRT",
        registrationDate: "15:56 1400/5/22",
        customer: "ابوالفضل آهنگر"
      },
      {
        id: 5,
        status: "تایید شده",
        price: "2,270,000,000 IRT",
        amount: "0.01017",
        market: "BTC-IRT",
        type: "قیمت بازار",
        filled: "75%",
        allAmount: "2,270,000,000 IRT",
        registrationDate: "19:21 1400/4/3",
        customer: "ابوالفضل آهنگر"
      },
      {
        id: 6,
        status: "تایید شده",
        price: "2,780,060,000 IRT",
        amount: "0.01034",
        market: "BTC-IRT",
        type: "قیمت دلخواه",
        filled: "80%",
        allAmount: "2,780,060,000 IRT",
        registrationDate: "17:54 1400/5/22",
        customer: "ابوالفضل آهنگر"
      }
    ],
    headersDone: [
      {
        text: "#",
        align: "center",
        sortable: false,
        value: "id"
      },
      {
        text: "تاریخ",
        value: "registrationDate",
        sortable: false,
        align: "center"
      },
      { text: "بازار", value: "market", sortable: false, align: "center" },
      {
        text: "خریدار/فروشنده",
        sortable: false,
        value: "customer",
        align: "center"
      },
      { text: "نوع", value: "type", sortable: false, align: "center" },
      { text: "قیمت", value: "price", sortable: false, align: "center" },
      { text: "مقدار", value: "amount", sortable: false, align: "center" },
      { text: "پر شده", value: "filled", sortable: false, align: "center" },
      {
        text: "مبلغ کل",
        value: "allAmount",
        sortable: false,
        align: "center"
      }
    ]
  },
  activeOrdersSell: [
    {
      coast: 3726790,
      amount: 0.0029,
      width: "21.3237%",
      price: 1285100000
    },
    {
      coast: 30942492,
      amount: 0.02407,
      width: "21.3237%",
      price: 1285000000
    },

    {
      coast: 1284900,
      amount: 0.001,
      width: "20.3237%",
      price: 1284100000
    },
    {
      coast: 20532000,
      amount: 0.016,
      width: "19.837%",
      price: 1283250000
    },
    {
      coast: 846517899,
      amount: 0.65977,
      width: "18.3237%",
      price: 1283050000
    },
    {
      coast: 5900420,
      amount: 0.0046,
      width: "17.3237%",
      price: 1282700000
    },
    {
      coast: 9941724,
      amount: 0.00776,
      width: "16.3237%",
      price: 1281150000
    },
    {
      coast: 3726790,
      amount: 0.0029,
      width: "14.3237%",
      price: 1285100000
    },
    {
      coast: 73793610,
      amount: 0.05764,
      width: "13.3237%",
      price: 1280250000
    },
    {
      coast: 47530113,
      amount: 0.03712,
      width: "12.3237%",
      price: 1280100000
    },
    {
      coast: 2764908,
      amount: 0.00216,
      width: "11.3237%",
      price: 128050000
    },
    {
      coast: 512000,
      amount: 0.004,
      width: "10.3237%",
      price: 1280000000
    },
    {
      coast: 5731264,
      amount: 0.00448,
      width: "9.3237%",
      price: 1279300000
    },
    {
      coast: 6203150,
      amount: 0.00458,
      width: "8.3237%",
      price: 1279000000
    },
    {
      coast: 27896694,
      amount: 0.02187,
      width: "7.3237%",
      price: 1275000000
    }
  ],
  activeOrdersBuy: [
    {
      coast: 25567200,
      amount: 0.0201,
      width: "21.3237%",
      price: 1272000000
    },
    {
      coast: 9118959,
      amount: 0.00718,
      width: "17.3237%",
      price: 1270050000
    },
    {
      coast: 35412680,
      amount: 0.02788,
      width: "16.3237%",
      price: 1270000000
    },
    {
      coast: 2844464,
      amount: 0.00224,
      width: "15.3237%",
      price: 1269850000
    },
    {
      coast: 6349900,
      amount: 0.005,
      width: "14.3237%",
      price: 1269800000
    },
    {
      coast: 199111368,
      amount: 0.15683,
      width: "13.3237%",
      price: 1269600000
    },
    {
      coast: 5076000,
      amount: 0.004,
      width: "11.3237%",
      price: 1269000000
    },
    {
      coast: 2534000,
      amount: 0.002,
      width: "10.3237%",
      price: 1267000000
    },
    {
      coast: 156216330,
      amount: 0.12345,
      width: "9.3237%",
      price: 1264500000
    },
    {
      coast: 3272336,
      amount: 0.00259,
      width: "8.3237%",
      price: 1263450000
    },
    {
      coast: 441910,
      amount: 0.00035,
      width: "7.3237%",
      price: 1262600000
    },
    {
      coast: 1262000,
      amount: 0.001,
      width: "6.3237%",
      price: 1262000000
    },
    {
      coast: 2231085,
      amount: 0.00177,
      width: "4.3237%",
      price: 1260500000
    },
    {
      coast: 126000,
      amount: 0.001,
      width: "3.3237%",
      price: 1260000010
    },
    {
      coast: 2771450,
      amount: 0.0022,
      width: "2.3237%",
      price: 1259750000
    }
  ],
  recentTrades: [
    {
      amount: "0.0002",
      price: "1210550000",
      houre: "16:13",
      isBuy: false
    },
    {
      amount: "0.0003",
      price: "1210100000",
      houre: "16:12",
      isBuy: false
    },
    {
      amount: "0.0007",
      price: "1210840960",
      houre: "16:12",
      isBuy: false
    },
    {
      amount: "0.0004",
      price: "1220740230",
      houre: "16:11",
      isBuy: true
    },
    {
      amount: "0.0004",
      price: "1220750010",
      houre: "16:10",
      isBuy: false
    },
    {
      amount: "0.0005",
      price: "1220950000",
      houre: "16:09",
      isBuy: true
    },
    {
      amount: "0.0009",
      price: "1221090000",
      houre: "16:08",
      isBuy: true
    },
    {
      amount: "0.0001",
      price: "1190890070",
      houre: "16:07",
      isBuy: true
    },
    {
      amount: "0.0002",
      price: "1210550000",
      houre: "16:06",
      isBuy: false
    },
    {
      amount: "0.0002",
      price: "1220100000",
      houre: "16:06",
      isBuy: false
    },
    {
      amount: "0.0003",
      price: "1220550000",
      houre: "16:05",
      isBuy: true
    },
    {
      amount: "0.0008",
      price: "1220550000",
      houre: "16:04",
      isBuy: false
    },
    {
      amount: "0.0004",
      price: "1204550000",
      houre: "16:04",
      isBuy: true
    },
    {
      amount: "0.0001",
      price: "1186550080",
      houre: "16:04",
      isBuy: false
    },
    {
      amount: "0.0002",
      price: "1193850000",
      houre: "16:03",
      isBuy: true
    },
    {
      amount: "0.0003",
      price: "1200810000",
      houre: "16:02",
      isBuy: true
    },
    {
      amount: "0.0004",
      price: "1220550000",
      houre: "16:01",
      isBuy: false
    },
    {
      amount: "0.0009",
      price: "1230000000",
      houre: "16:00",
      isBuy: false
    },
    {
      amount: "0.0003",
      price: "1190550000",
      houre: "15:59",
      isBuy: true
    },
    {
      amount: "0.0008",
      price: "1200550000",
      houre: "15:58",
      isBuy: true
    },
    {
      amount: "0.0004",
      price: "1220550000",
      houre: "16:01",
      isBuy: false
    },
    {
      amount: "0.0009",
      price: "1230000000",
      houre: "16:00",
      isBuy: false
    },
    {
      amount: "0.0003",
      price: "1190550000",
      houre: "15:59",
      isBuy: true
    },
    {
      amount: "0.0008",
      price: "1200550000",
      houre: "15:58",
      isBuy: true
    },
    {
      amount: "0.0003",
      price: "1190550000",
      houre: "15:59",
      isBuy: false
    }
  ],
  markets: {
    allMarkets: [
      {
        name: "TOMAN/BTC",
        price: "1226400000",
        changes: "3.4%",
        market: "BTCTOMAN"
      },
      // {
      //   name: "TOMAN/DOGE",
      //   price: "5765",
      //   changes: "4.565%",
      //   market: "DOGETOMAN"
      // },
      // {
      //   name: "TOMAN/LTC",
      //   price: "47390000",
      //   changes: "0.36%",
      //   market: "LTCTOMAN"
      // },
      // {
      //   name: "TOMAN/DASH",
      //   price: "1766",
      //   changes: "-0.38%",
      //   market: "DASHTOMAN"
      // }
    ],
    marketIRT: [
      {
        name: "TOMAN/BTC",
        price: "1226400000",
        changes: "3.4%",
        market: "BTCTOMAN"
      },
      // {
      //   name: "TOMAN/DOGE",
      //   price: "5765",
      //   changes: "4.565%",
      //   market: "DOGETOMAN"
      // },
      // {
      //   name: "TOMAN/LTC",
      //   price: "47390000",
      //   changes: "0.36%",
      //   market: "LTCTOMAN"
      // },
      // {
      //   name: "TOMAN/DASH",
      //   price: "1766",
      //   changes: "-0.38%",
      //   market: "DASHTOMAN"
      // }
    ],
    marketUSDT: [
      {
        name: "BTC/USDT",
        price: "0",
        changes: "0%"
      },

      {
        name: "ETH/USDT",
        price: "1,766",
        changes: "-0.38%"
      }
    ]
  }
};

mock
  .onGet(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.openOrders}`)
  .reply(() => {
    return [200, dashboard.openOrders];
  });

mock
  .onGet(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.cancelOrders}`)
  .reply(() => {
    return [200, dashboard.cancelOrders];
  });
mock
  .onGet(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.doneOrders}`)
  .reply(() => {
    return [200, dashboard.doneOrders];
  });

mock
  .onGet(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.activeOrdersSell}`)
  .reply(() => {
    return [200, dashboard.activeOrdersSell];
  });
mock
  .onGet(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.activeOrdersBuy}`)
  .reply(() => {
    return [200, dashboard.activeOrdersBuy];
  });

mock
  .onGet(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.recentTrades}`)
  .reply(() => {
    return [200, dashboard.recentTrades];
  });

mock
  .onGet(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.allMarkets}`)
  .reply(() => {
    return [200, dashboard.markets.allMarkets];
  });
mock
  .onGet(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.marketIRT}`)
  .reply(() => {
    return [200, dashboard.markets.marketIRT];
  });
mock
  .onGet(`${constants.DASHBOARD_BASE_ENDPOINT}${constants.DASHBOARD_ENDPOINTS.marketUSDT}`)
  .reply(() => {
    return [200, dashboard.markets.marketUSDT];
  });

mock.onGet("/api/test").reply(() => {
  return [200, { message: "ok" }];
});
