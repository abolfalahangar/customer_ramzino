import {  mount, createLocalVue } from "@vue/test-utils";
import Vuetify from "vuetify";
import VueRouter from "vue-router";
import Vue from "vue";
import Vuex from "vuex";
import pathify from "@/plugins/vuex-pathify";
import * as modules from "@/store/modules";
import DepositRecords from "@/views/deposit-records/DepositRecords";

import "@/plugins/globalMethods";
import "@/plugins/globalComponents";
import "@/api/api";
// import {
//   DEPOSIT_RECORDS_BASE_ENDPOINT,
//   DEPOSIT_RECORDS_ENDPOINTS
// } from "@/api/constants";

Vue.use(Vuetify);

jest.mock("axios", () => ({
  create: jest.fn(() => ({
    get: jest.fn(() =>
      Promise.resolve({
        data: {
          depositRecords: [],
          headers: []
        }
      })
    ),
    interceptors: {
      request: {
        use: jest.fn(),
        eject: jest.fn()
      },
      response: {
        use: jest.fn(),
        eject: jest.fn()
      }
    },
    headers: {}
  }))
}));

describe("deposit records", () => {
  let localVue;
  let store;
  let router;
  let vuetify;
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(VueRouter);
    localVue.use(Vuex);
    router = new VueRouter();
    vuetify = new Vuetify();
    store = new Vuex.Store({
      modules,
      plugins: [pathify.plugin]
    });
    jest.resetModules();
    jest.clearAllMocks();
  });

  test("DepositRecords api", () => {
    const wrapper = mount(DepositRecords, {
      localVue,
      router,
      vuetify,
      store
    });

    //jest.spyOn(wrapper, "getDepositRecords");

    const title = wrapper.findComponent({ name: "v-toolbar-title" });
    expect(title.text()).toEqual("سوابق واریز");
    const table = wrapper.findComponent({ name: "v-data-table" });
    expect(table.exists()).toBe(true);
    //const url = `${DEPOSIT_RECORDS_BASE_ENDPOINT}${DEPOSIT_RECORDS_ENDPOINTS.depositRecords}`;
    // wrapper.vm.$api.DepositRecords.getDepositRecords = jest.fn();
    // expect(wrapper.vm.$api.DepositRecords.getDepositRecords).toHaveBeenCalled();
  });
});
