import {  mount, createLocalVue } from "@vue/test-utils";
import Vuetify from "vuetify";
import VueRouter from "vue-router";
import Vue from "vue";
import Vuex from "vuex";
import pathify from "@/plugins/vuex-pathify";
import DashboardCards from "@/components/dashboard/DashboardCards.vue";
import "@/plugins/globalMethods";
import "@/plugins/globalComponents";
import "@/plugins/directives";
import "@/api/api";
import * as modules from "@/store/modules";
import axios from "axios";

Vue.use(Vuetify);

jest.mock("axios", () => ({
  create: jest.fn(() => ({
    get: jest.fn(() =>
      Promise.resolve({
        data: {
          userId: 1,
          id: 1,
          title:
            "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
          body:
            "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
        }
      })
    ),
    interceptors: {
      request: {
        use: jest.fn(),
        eject: jest.fn()
      },
      response: {
        use: jest.fn(),
        eject: jest.fn()
      }
    },
    headers: {}
  }))
}));

describe("dashboard cards", () => {
  let localVue;
  let store;
  let router;
  let vuetify;
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(VueRouter);
    localVue.use(Vuex);
    router = new VueRouter();
    vuetify = new Vuetify();
    store = new Vuex.Store({
      modules,
      plugins: [pathify.plugin]
    });
    jest.resetModules();
    jest.clearAllMocks();
  });

  describe("test api OrderCards", () => {
    test("send post buy request with data", async () => {
      let data = {
        userId: 1,
        id: 1,
        title:
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        body:
          "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
      };
      const wrapper = mount(DashboardCards, {
        localVue,
        router,
        vuetify,
        store
      });

      await wrapper.vm.postBuy();
      expect(wrapper.vm.fake).toEqual(data);
      const url = `http://jsonplaceholder.typicode.com/posts/1`;
      expect(wrapper.vm.$http.get).toHaveBeenCalledWith(url);
    });

    // test("post sell data to back", async () => {
    //   const wrapper = mount(DashboardCards, {
    //     localVue,
    //     router,
    //     vuetify,
    //     store
    //   });

    //   let data = {
    //     userId: 1,
    //     id: 2,
    //     title: "qui est esse",
    //     body:
    //       "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
    //   };

    //   await wrapper.vm.postSell();
    //   expect(wrapper.vm.fake).toEqual(data);
    //   const url = `http://jsonplaceholder.typicode.com/posts/2`;
    //   expect(wrapper.vm.$http.get).toHaveBeenCalledWith(url);
    // });

    // test("post sell data to back end", async () => {
    //   const wrapper = mount(DashboardCards, {
    //     localVue,
    //     router,
    //     vuetify,
    //     store
    //   });

    //   await wrapper.vm.postSell();
    //   let data = {
    //     unitPrice,
    //     amount,
    //     allAmount
    //   };

    //   let url = "";
    //   expect(wrapper.vm.$http.get).toHaveBeenCalledWith(url);

    //   expect(wrapper.vm.$http.post).toHaveBeenCalledWith(
    //     expect.objectContaining(data)
    //   );
    // });
  });
});
