import {  mount, createLocalVue } from "@vue/test-utils";
import Vuetify from "vuetify";
import VueRouter from "vue-router";
import Vue from "vue";
import Vuex from "vuex";
import pathify from "@/plugins/vuex-pathify";
import Dashboard from "@/views/dashboard/Dashboard.vue";
import "@/plugins/globalMethods";
import "@/plugins/globalComponents";
import "@/plugins/directives";
import "@/api/api";
import * as modules from "@/store/modules";

Vue.use(Vuetify);

jest.mock("axios", () => ({
  create: jest.fn(() => ({
    get: jest.fn(() =>
      Promise.resolve({
        data: {
          userId: 1,
          id: 1,
          title:
            "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
          body:
            "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
        }
      })
    ),
    interceptors: {
      request: {
        use: jest.fn(),
        eject: jest.fn()
      },
      response: {
        use: jest.fn(),
        eject: jest.fn()
      }
    },
    headers: {}
  }))
}));

window.Notification = { 
    requestPermission : function(){},
    permission:""
}

window.TradingView ={
    widget:function(){},
}


describe("Dashboard view", () => {
  let localVue;
  let store;
  let router;
  let vuetify;

  beforeEach(() => {
     localVue = createLocalVue();
     localVue.use(VueRouter);
     localVue.use(Vuex);
     router = new VueRouter();
     vuetify = new Vuetify();
     store = new Vuex.Store({
       modules,
       plugins: [pathify.plugin]
     });
     jest.resetModules();
     jest.clearAllMocks();
  });

    test("Dashboard view UI", () => {
     const wrapper = mount(Dashboard, {
            localVue,
            router,
            vuetify,
            store
     });
     

      expect(wrapper.html()).toMatchSnapshot();
      const Markets = wrapper.findComponent({ name: "markets" });
      const BoardMarket = wrapper.findComponent({ name: "board-market" });
      const DashboardCards = wrapper.findComponent({ name: "dashboard-cards" });
      const OrderBook = wrapper.findComponent({ name: "order-book" });
      const tv = wrapper.findComponent({ name: "tv" });
      const TableOrders = wrapper.findComponent({ name: "table-orders" });

      expect(Markets.exists()).toBe(true);
      expect(RecentTrades.exists()).toBe(true);
      expect(BoardMarket.exists()).toBe(true);
      expect(DashboardCards.exists()).toBe(true);
      expect(OrderBook.exists()).toBe(true);
      expect(tv.exists()).toBe(true);
      expect(TableOrders.exists()).toBe(true);
    
    })
})