import {  mount, createLocalVue } from "@vue/test-utils";
import Vuetify from "vuetify";
import VueRouter from "vue-router";
import Vue from "vue";
import Vuex from "vuex";
import pathify from "@/plugins/vuex-pathify";
import * as modules from "@/store/modules";
import OpenOrders from "@/views/open-orders/OpenOrders";
import "@/plugins/globalMethods";
import "@/plugins/globalComponents";
import "@/api/api";

Vue.use(Vuetify);

jest.mock("axios", () => ({
  create: jest.fn(() => ({
    get: jest.fn(() =>
      Promise.resolve({
        data: {
          userId: 1,
          id: 2,
          title: "qui est esse",
          body:
            "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
        }
      })
    ),
    interceptors: {
      request: {
        use: jest.fn(),
        eject: jest.fn()
      },
      response: {
        use: jest.fn(),
        eject: jest.fn()
      }
    },
    headers: {}
  }))
}));

describe("Open Orders", () => {
  let localVue;
  let store;
  let router;
  let vuetify;
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(VueRouter);
    localVue.use(Vuex);
    router = new VueRouter();
    vuetify = new Vuetify();
    store = new Vuex.Store({
      modules,
      plugins: [pathify.plugin]
    });
    jest.resetModules();
    jest.clearAllMocks();
  });

  test("Open Orders UI", () => {
    const wrapper = mount(OpenOrders, {
      localVue,
      router,
      vuetify,
      store
    });
    expect(wrapper.html()).toMatchSnapshot();
    const table = wrapper.findComponent({ name: "v-data-table" });
    const title = wrapper.findComponent({ name: "v-toolbar-title" });
    expect(table.exists()).toBe(true);
    expect(title.text()).toEqual("سفارشات باز");
   
  },3000);
  
  test("jsonplaceholder ", () => {
    const wrapper = mount(OpenOrders, {
      localVue,
      router,
      vuetify,
      store
    });

    const data = {
      userId: 1,
      id: 2,
      title: "qui est esse",
      body:
        "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
    };

    const url = `http://jsonplaceholder.typicode.com/posts/2`;
    wrapper.vm.$api.OpenOrders.jsonPlaceholder().then(res => {
  
      expect(wrapper.vm.$http.get).toHaveBeenCalledWith(url);
      expect(wrapper.vm.fake).toEqual(data);
    });
  });


   // jest.useFakeTimers();
  // test("Open Orders should be updated with good", () => {
  //   const wrapper = mount(OpenOrders, {
  //     localVue,
  //     router,
  //     vuetify,
  //     store
  //   });

  //   jest.runAllTimers();
  //   const title = wrapper.findComponent({ name: "v-toolbar-title" });
  //   expect(title.text()).toEqual("سفارشات باز");
  //   wrapper.vm.$nextTick();
  //   expect(wrapper.html()).toMatchSnapshot();
  // });
});
